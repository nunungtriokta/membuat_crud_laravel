@extends('layouts.app')

@section('content')
<h4>{{ $foodPlace->nama }}</h4>
<p>{{ $foodPlace->alamat }}</p>
<p>{{ $foodPlace->latitude }}</p>
<p>{{ $foodPlace->longitude }}</p>
<a href="{{ route('foodplace.index') }}" class="btn btn-default">Kembali</a>
@endsection